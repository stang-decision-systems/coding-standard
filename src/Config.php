<?php

declare(strict_types=1);

namespace Stang\CodingStandard;

use PhpCsFixer\Config as BaseConfig;

class Config extends BaseConfig
{
    public function __construct($name = 'stang')
    {
        parent::__construct($name);
    }

    public function getRules(): array
    {
        return array_merge(require __DIR__ . '/rules.php', parent::getRules());
    }
}
