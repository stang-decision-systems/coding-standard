<?php declare(strict_types=1);

use PhpCsFixer\Finder;
use Stang\CodingStandard\Config;

$finder = Finder::create()->in('src');

return (new Config)
    ->setRiskyAllowed(true)
    ->setFinder($finder)
;
